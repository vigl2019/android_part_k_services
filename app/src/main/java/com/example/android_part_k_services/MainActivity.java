package com.example.android_part_k_services;

/*

User story:

Вася работает курьером, бегает по утрам, ведёт активный образ жизни.
Федя работает программистом и любит в свободное время играть в компьютерные игры.

Приложение должно определять образ жизни хозяина девайса.

На экране должно быть два цвета: красный и зелёный.
Чем активнее образ жизни человека - тем больше зелёного.
Можно считать, что для Васи должно быть 90% зелёного, для Феди 90% красного.

*/

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android_part_k_services.model.PersonActivity;
import com.example.android_part_k_services.utils.AppHelper;
import com.example.android_part_k_services.utils.Helper;
import com.example.android_part_k_services.utils.IHelper;
import com.example.android_part_k_services.utils.IPersonActivityTypes;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.green_color)
    LinearLayout green;

    @BindView(R.id.red_color)
    LinearLayout red;

    @BindView(R.id.move_percent)
    TextView movePercent;

    @BindView(R.id.sit_percent)
    TextView sitPercent;

    private Helper helper;
    PersonActivityDatabase database;
    private CompositeDisposable disposables = new CompositeDisposable();

    //================================================================================//

    public interface OnActionGenerate{
        void action(boolean isServiceFinishWork);
    }

    boolean isBinded;
    PersonActivityService personActivityService;

    OnActionGenerate onActionGenerate = new OnActionGenerate() {
        @Override
        public void action(boolean isServiceFinishWork) {

            if(isServiceFinishWork) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        database = AppHelper.getInstance().getDatabase();
                        helper = new Helper(MainActivity.this, database);
                        helper.getPersonActivityListFromDataBase(iHelper);
                    }
                });
            }
            else{
                Helper.createAlertDialog("Service hasn't finished it work yet!", null, MainActivity.this);
            }
        }
    };

    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            PersonActivityService.MyBinder binder = (PersonActivityService.MyBinder) iBinder;
            personActivityService = binder.getService();
            personActivityService.setOnActionGenerate(onActionGenerate);
            personActivityService.startTask();
            isBinded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBinded = false;
        }
    };

    //================================================================================//

    private IHelper iHelper = new IHelper() {
        @Override
        public void getPersonActivityList(List<PersonActivity> personActivityList) {
            drawPersonActivity(personActivityList);
        }
    };

    //================================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, PersonActivityService.class);
        bindService(intent, conn, BIND_AUTO_CREATE);
    }

    //================================================================================//

    private void drawPersonActivity(List<PersonActivity> personActivityList) {

        // amount of records = 100%
        // 'sit' = 0
        // 'move' = 1

        int sitCount = 0;
        int moveCount = 0;

        for (PersonActivity personActivity : personActivityList) {
            if (personActivity.getActivityType() == IPersonActivityTypes.PersonActivityTypes.SIT.ordinal()) {
                sitCount++;
            }
            if (personActivity.getActivityType() == IPersonActivityTypes.PersonActivityTypes.MOVE.ordinal()) {
                moveCount++;
            }
        }

        //================================================================================//

        // fill layouts with appropriate color

        float sitColorPercent = 0;
        float moveColorPercent = 0;

        if (moveCount != 0) {
            sitColorPercent = (sitCount / moveCount) * 100;
            moveColorPercent = 100 - sitColorPercent;
        } else {
            sitColorPercent = 100;
        }

        green.setWeightSum(sitColorPercent);
        red.setWeightSum(moveColorPercent);

        movePercent.setText("Person moved " + String.valueOf(moveColorPercent) + "% of the time in the last 24 hours");
        sitPercent.setText("Person sat " + String.valueOf(sitColorPercent) + "% of the time in the last 24 hours");
    }

    //================================================================================//

    @Override
    protected void onStop(){
        super.onStop();

        personActivityService.setOnActionGenerate(null);
        unbindService(conn);

        helper.unSubscribe();
    }
}


