package com.example.android_part_k_services.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class PersonActivity {

    @PrimaryKey
    private int id;

    private long date;
    private int activityType;

    public PersonActivity(int id, long date, int activityType) {
        this.id = id;
        this.date = date;
        this.activityType = activityType;
    }

    public int getId() {
        return id;
    }

    public long getDate() {
        return date;
    }

    public int getActivityType() {
        return activityType;
    }
}