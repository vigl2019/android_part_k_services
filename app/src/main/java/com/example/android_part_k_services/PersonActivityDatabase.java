package com.example.android_part_k_services;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.example.android_part_k_services.model.PersonActivity;

@Database(entities=PersonActivity.class, version=1)
public abstract class PersonActivityDatabase extends RoomDatabase {
    public abstract PersonActivityDao getPersonActivityDao();
}