package com.example.android_part_k_services.utils;

public interface IPersonActivityDataReady {
    void isPersonActivityDataReady(boolean isDataReady);
}
