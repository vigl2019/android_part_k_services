package com.example.android_part_k_services.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.android_part_k_services.PersonActivityDatabase;
import com.example.android_part_k_services.R;
import com.example.android_part_k_services.model.PersonActivity;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class Helper {

    private Context context;
    private static PersonActivityDatabase database;
    private CompositeDisposable disposables = new CompositeDisposable();

    private static final int personActivityInterval = 24 * 6; // 24 hours at 6 intervals every hour

    public Helper(Context context, PersonActivityDatabase database) {
        this.context = context;
        this.database = database;
    }

    //================================================================================//

    public static void generatePersonActivityData() {

        int id = database.getPersonActivityDao().getLastId();
        Calendar date = new GregorianCalendar();
        int activityType = (int) Math.random() * 2;

        database.getPersonActivityDao().insertPersonActivity(new PersonActivity(id + 1, date.getTimeInMillis(), activityType));
    }

    public static void generatePersonActivityDataByInterval(int interval) {

        int activityType = 0;
        Calendar date = new GregorianCalendar();

        for (int i = 0; i < interval; i++) {
            activityType = (int) Math.random() * 2;
            database.getPersonActivityDao().insertPersonActivity(new PersonActivity(i + 1, date.getTimeInMillis(), activityType));
        }
    }

    //================================================================================//

    public void getPersonActivityListFromDataBase(final IHelper iHelper) {

        disposables.add(database.getPersonActivityDao().getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<PersonActivity>>() {
                    @Override
                    public boolean test(List<PersonActivity> personActivityList) throws Exception {

                        boolean ispersonActivityDBListHaveValues = ((personActivityList != null) && (!personActivityList.isEmpty()));

                        if (!ispersonActivityDBListHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_person_activity_db_list), null, context);
                        }

                        return ispersonActivityDBListHaveValues;
                    }
                })

                .subscribe(new Consumer<List<PersonActivity>>() {
                    @Override
                    public void accept(List<PersonActivity> personActivityList) throws Exception {
                        iHelper.getPersonActivityList(personActivityList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
//                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void trackPersonActivity(final long startTimeTracking, final long endTimetracking, final IPersonActivityDataReady iPersonActivityDataReady){

        Flowable.interval(1, TimeUnit.MINUTES)

                .takeUntil(new Predicate<Long>() {
                    @Override
                    public boolean test(Long aLong) throws Exception {
                        Calendar timeNow = new GregorianCalendar();

                        if ((startTimeTracking + endTimetracking) >= timeNow.getTimeInMillis()) {
                            iPersonActivityDataReady.isPersonActivityDataReady(true);
                            return true;
                        }
                        else {
                            iPersonActivityDataReady.isPersonActivityDataReady(false);
                            return false;
                        }
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {

                        // write values to the database
                        Helper.generatePersonActivityData();

//                      sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//                      sensorManager.registerListener(PersonActivityService.this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);

                    }
                });
    }

    //================================================================================//

/*
    // number >= 0.5 and <= 1
    public static double generateSitActivityType(){

    }

    // number >= 4 and <= 6
    public static int generateMoveActivityType(){

    }
*/

//================================================================================//

    public void unSubscribe() {
        disposables.clear();
    }

    //================================================================================//

    public static void createAlertDialog(String errorMassage, final View view, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();

                        if (view != null) {
                            view.requestFocus();
                        }
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}



