package com.example.android_part_k_services.utils;

import android.app.Application;

import androidx.room.Room;

import com.example.android_part_k_services.PersonActivityDatabase;

public class AppHelper extends Application {
    public static AppHelper instance;

    private PersonActivityDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, PersonActivityDatabase.class, "PersonActivityDatabase")
                .build();
    }

    public static AppHelper getInstance() {
        return instance;
    }

    public PersonActivityDatabase getDatabase() {
        return database;
    }
}
