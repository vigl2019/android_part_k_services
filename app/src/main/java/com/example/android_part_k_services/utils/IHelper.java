package com.example.android_part_k_services.utils;

import com.example.android_part_k_services.model.PersonActivity;

import java.util.List;

public interface IHelper {
    void getPersonActivityList(List<PersonActivity> personActivityList);
}
