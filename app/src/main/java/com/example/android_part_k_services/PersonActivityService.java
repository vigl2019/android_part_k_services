package com.example.android_part_k_services;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.example.android_part_k_services.model.PersonActivity;
import com.example.android_part_k_services.utils.AppHelper;
import com.example.android_part_k_services.utils.Helper;
import com.example.android_part_k_services.utils.IPersonActivityTypes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class PersonActivityService extends Service implements SensorEventListener {

    private long startTimeTracking; // start tracking person's activity
//  private static final long END_TIME_TRACKING = 86_400_000; // 24 hours in milliseconds
    private static final long END_TIME_TRACKING = 600_000; // 10 minutes in milliseconds
    private SensorManager sensorManager;
    private PersonActivityDatabase database;
    private CompositeDisposable disposables = new CompositeDisposable();
    private Helper helper;
    private boolean isFinish = false;

    //================================================================================//

    MyBinder myBinder = new MyBinder();

    class MyBinder extends Binder {
        PersonActivityService getService() {
            return PersonActivityService.this;
        }
    }

    private MainActivity.OnActionGenerate onActionGenerate;

    public void setOnActionGenerate(MainActivity.OnActionGenerate onActionGenerate) {
        this.onActionGenerate = onActionGenerate;
    }

    //================================================================================//

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startTimeTracking = new GregorianCalendar().getTimeInMillis();
        startTask();
        return super.onStartCommand(intent, flags, startId);
    }

    //================================================================================//

    public void startTask() {
        if (onActionGenerate != null) {

//          trackPersonActivity();
//          onActionGenerate.action(isFinish);

            onActionGenerate.action(trackPersonActivity());
        }
    }

    //================================================================================//

    public boolean trackPersonActivity() {

        for(int i = 0; i <= (24 * 6); i++) {
            // write values to the database
            Helper.generatePersonActivityData();
        }

        return true;

/*
        IPersonActivityDataReady iPersonActivityDataReady = new IPersonActivityDataReady() {
            @Override
            public void isPersonActivityDataReady(boolean isDataReady) {
                if(isDataReady){
                    isFinish = true;
                }
            }
        };

        helper.trackPersonActivity(startTimeTracking, startTimeTracking + END_TIME_TRACKING, iPersonActivityDataReady);
*/
    }
/*
    Predicate<Long> takeUntilPredicate = new Predicate<Long>() {
        @Override
        public boolean test(Long time) throws Exception {
            Calendar timeNow = new GregorianCalendar();

            if ((time + END_TIME_TRACKING) >= timeNow.getTimeInMillis()) {
                return true;
            }
            else {
                return false;
            }
        }
    };
*/

    //================================================================================//

    class Coordinates {
        double ax;
        double ay;
        double az;

        public Coordinates(double ax, double ay, double az) {
            this.ax = ax;
            this.ay = ay;
            this.az = az;
        }

        public double getAx() {
            return ax;
        }

        public double getAy() {
            return ay;
        }

        public double getAz() {
            return az;
        }
    }

    //================================================================================//

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            int activityType = -1;

            List<Coordinates> coordinatesList = new ArrayList<>();
            coordinatesList.add(new Coordinates(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]));

            if (coordinatesList.size() > 50) {

                sensorManager.unregisterListener(this);

                List<Double> results = new ArrayList<>();

                for (Coordinates coordinates : coordinatesList) {

                    double ax = coordinates.getAx();
                    double ay = coordinates.getAy();
                    double az = coordinates.getAz();

                    double v = (ax * ax) + (ay * ay) + (az * az);
                    double result = Math.sqrt(v);

                    results.add(result);
                }

                double min = results.get(0);
                for (int i = 1; i < results.size(); i++) {
                    if (results.get(i) < min) {
                        min = results.get(i);
                    }
                }

                double max = results.get(0);
                for (int i = 1; i < results.size(); i++) {
                    if (results.get(i) > max) {
                        max = results.get(i);
                    }
                }

                double difference = max - min;

                if (difference >= 0.5 && difference <= 1) {
                    activityType = IPersonActivityTypes.PersonActivityTypes.SIT.ordinal();
                }
                if (difference >= 4 && difference <= 6) {
                    activityType = IPersonActivityTypes.PersonActivityTypes.MOVE.ordinal();
                }

                Calendar date = new GregorianCalendar();

                // write values to the database
                database = AppHelper.getInstance().getDatabase();

                int lastId = database.getPersonActivityDao().getLastId();
                PersonActivity personActivity = new PersonActivity(lastId + 1, date.getTimeInMillis(), activityType);
                database.getPersonActivityDao().insertPersonActivity(personActivity);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
