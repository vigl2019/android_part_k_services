package com.example.android_part_k_services;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.android_part_k_services.model.PersonActivity;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class PersonActivityDao {

    @Insert
    public abstract void insertPersonActivity(PersonActivity personActivity);

    @Query("SELECT * FROM PersonActivity")
    public abstract Flowable<List<PersonActivity>> getAll();

    @Query("SELECT last_insert_rowid()")
    public abstract int getLastId();
}
